package fi.vamk.e1700678;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static String helloWeekday(String day){
        try{
            // need to convert a string to a date object
            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(day);

            // need to use the date create a calendar object
            Calendar c = Calendar.getInstance();
            c.setTime(date1);

            // use calendar object to get day of the week
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            // use separate function to get a weekday string based of the int
            // value of the weekday
            String wDay = weekDay(dayOfWeek);

            return("Hello " + wDay + " World!");
        }catch (ParseException | NullPointerException e){
            e.printStackTrace();
            return "Hello World!";
        }
    }

    public static String weekDay(int day){
        String wd;
        switch (day){
            case 1: wd = "Sunday";
                    break;
            case 2: wd = "Monday";
                    break;
            case 3: wd = "Tuesday";
                    break;
            case 4: wd = "Wednesday";
                    break;
            case 5: wd = "Thursday";
                    break;
            case 6: wd = "Friday";
                    break;
            case 7: wd = "Saturday";
                    break;
            default: wd = "";
                    break;
        }
        return wd;
    }

}
