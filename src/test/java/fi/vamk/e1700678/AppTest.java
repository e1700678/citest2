package fi.vamk.e1700678;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void helloThursday(){
        String day = App.helloWeekday("19/03/2020");
        assertEquals("Hello Thursday World!", day);
    }

    @Test
    public void helloMonday(){
        String day = App.helloWeekday("16/03/2020");
        assertEquals("Hello Monday World!", day);
    }

    @Test
    public void helloTuesday(){
        String day = App.helloWeekday("17/03/2020");
        assertEquals("Hello Tuesday World!", day);
    }

    @Test
    public void helloLeapday(){
        String day = App.helloWeekday("29/02/2020");
        assertEquals("Hello Saturday World!", day);
    }

    @Test
    public void helloSunday(){
        String day = App.helloWeekday("15/03/2020");
        assertEquals("Hello Sunday World!", day);
    }

    @Test
    public void helloEmpty(){
        String day = App.helloWeekday("");
        assertEquals("Hello World!", day);
    }

    @Test
    public void helloNull(){
        String day = App.helloWeekday(null);
        assertEquals("Hello World!", day);
    }

}
